(use-modules (ice-9 format)
             (ice-9 popen)
             (ice-9 textual-ports))

;; (let* ((port (open-input-pipe "./org-parse.sh test.org"))
;;        (parsed (get-string-all port)))
;;   (close-pipe port)
;;   (display parsed)
;;   parsed)

(define work-dir (dirname (current-filename)))

(define (to-work-dir f)
  (format "~a/~a" (dirname (current-filename)) f))

(define (parse-org-file f)
  (let* ((port (open-input-pipe (format "~a ~s" (to-work-dir "org-parse.sh") f)))
         (parsed-r (read port)))
    (close-pipe port)
    parsed-r))

(display (parse-org-file (to-work-dir "test.org")))
