#!/usr/bin/env bash
set -euo pipefail

FILE=$1

DIR=$(dirname ${BASH_SOURCE[0]})

emacs -batch -L ${DIR} -l org -l org-parse --eval "(org-parse \"${FILE}\")"
