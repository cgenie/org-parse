# org-parse

The aim of this project is to parse an Emacs Org file and output it as an
S-expression. This basically uses Emacs org mode `org-element-parse-buffer` and
removes any recursive references so that hopefully this can be read by other
lisps easily. We aim to be as close to the output of `org-element-parse-buffer`
as possible.

Main code is in [`org-parse.el`](./org-parse.el).

## Details

The description of the output of `org-element-parse-buffer` can be found eg.
[here](http://ergoemacs.org/emacs/elisp_parse_org_mode.html) (look for
`(org-data)`).

Unfortunately, when this is printed as a string, it is not directly parseable by
Scheme nor Lisp. I.e. this naive code:
``` emacs-lisp
(with-temp-buffer
  (find-file f))
  (print (org-element-parse-buffer))
```
outputs something that can't be parsed, say, by GNU Guile.

From what I observed, most issues are because of Emacs' [text
properties](https://www.gnu.org/software/emacs/manual/html_node/elisp/Text-Properties.html).
When one `print`s this in Emacs, the output looks like this:
``` emacs-lisp
#("foo" 0 4 (fontified t face font-lock-function-name-face))
```
and is not directly parseable by Scheme nor Common Lisp. For such cases, we use
`substring-no-properties`.

Also, there are recursive elements in the output of `org-element-parse-buffer`.
The ones I found so far are in the `:parent` props (printed simply as `#0`, `#1`
etc). I just set them to `nil` as it can be easily inspected from the
S-expression already what the parent is.

## Testing

To see the parsed output run:
``` sh
./org-parse.sh test.org
```
The [`org-parse.sh`](./org-parse.sh) script is just a simple wrapper over `emacs -batch`.

To see how it can be used, say, in [Guile](https://www.gnu.org/software/guile/), run:
``` sh
guile parse.scm
```

## Common Lisp

In [Common Lisp](https://common-lisp.net/) I do something like:

``` common-lisp
(ql:quickload 'inferior-shell)

(defparameter org-parse-el-dir "<wherever this repo is cloned>")
(defparameter org-file "<whatever your org file is>")
(defparameter org-parse-code (format nil "(org-parse ~s)" org-file))
(defparameter emacs-cmd `(emacs -batch "-L" ,org-parse-el-dir -l org -l org-parse --eval ,org-parse-code))

(defparameter org-str (inferior-shell:run/s emacs-cmd))
(defparameter org-parsed (read-from-string org-str))

;; By now, hopefully your org file is read correctly
;; You can do basic things like:

(defun find-items-of-type (type items)
  (flet ((itemp (i)
           (eq (get-org-item-type i) type)))
    (remove-if-not #'itemp items)))

(defun get-org-item-type (item)
  (car item))

(defun get-org-props (item)
  (cadr item))

(defun get-org-children (item)
  (cddr item))


(defun get-org-children-with-parent (item)
  (mapcar (lambda (c) (list item c))
          (get-org-children item)))

(defun find-children-of-type (type item)
  (find-item-of-type type (get-org-children item)))
```
