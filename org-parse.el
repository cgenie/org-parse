;;; org-parse.el --- description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2020 Przemysław Kamiński
;;
;; Author: Przemysław Kamiński <https://gitlab.com/cgenie>
;; Maintainer: Przemysław Kamiński <pk@intrepidus.pl>
;; Created: September 15, 2020
;; Modified: September 15, 2020
;; Version: 0.0.1
;; Keywords:
;; Homepage: https://gitlab.com/cgenie/org-parse
;; Package-Requires: ((emacs 27.1) (cl-lib "0.5"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  description
;;
;;; Code:

(require 'org)

(defun fix-parent (props)
  "Fix recursive references found in :parent PROPS."
  (pcase props
    (`(:parent ,parent) '())
    (`(:parent ,parent . ,_) (cddr props))
    (`(,k ,v . ,_) (append (list k v) (fix-parent (cddr props))))
    (`() '())))

(defun fix-text-properties (props)
  "Fix strings with properties found in PROPS."
  (pcase props
    (`(:caption ,caption) '(substring-no-properties caption))
    (`(:caption ,caption . ,_) (append (list :caption (substring-no-properties caption)) (cddr props)))
    (`(:title ,title) '(substring-no-properties title))
    (`(:title ,title . ,_) (append (list :title (substring-no-properties title)) (cddr props)))
    (`(,k ,v . ,_) (append (list k v) (fix-text-properties (cddr props))))
    (`() '())))

(defun fix-props (props)
  "Apply all fixes to PROPS."
  (fix-text-properties (fix-parent props)))

(defun simple-strip-string (item)
  "Apply `substring-no-properties' to ITEM, if it is a string."
  (pcase item
    ((pred stringp) (substring-no-properties item))
    (_ item)))

(defun strip-string-text (item)
  "Strip ITEM's text, which sometimes appears as the 3rd element."
  (pcase item
    (`(,type ,props ,textp . ,rest)
     (if (stringp textp)
         (append (list type props (substring-no-properties textp)) (mapcar #'simple-strip-string rest))
         (append (list type props textp) (mapcar #'simple-strip-string rest))))
    (_ item)))

(defun fix-item (item)
  "Apply S-expresion fixes to ITEM."
  (let ((item- (strip-string-text item)))
    (pcase item-
      (`(,type ,props . ,rest) (append (list type (fix-props props)) (mapcar #'fix-item rest)))
      (_ item-))))

(defun org-parse (f)
  "Main function to parse the file F."
  (with-temp-buffer
    (find-file f)
    (let* ((parsed (org-element-parse-buffer))
           (all (append org-element-all-elements org-element-all-objects))
           (mapped- (org-element-map parsed all
                      (lambda (item)
                        (fix-item item))))
           (mapped (fix-item parsed)))
      ;; (pp mapped)
      (print mapped))))

(provide 'org-parse)
;;; org-parse.el ends here
