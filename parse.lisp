(ql:quickload 'cl-fad)
(ql:quickload 'cl-json)
(ql:quickload 'inferior-shell)

(defun current-filename ()
  "/home/przemek/git-work/github/EMACS/org-parse/parse.lisp")

(defparameter work-dir (cl-fad:pathname-directory-pathname (current-filename)))

(defun to-work-dir (f)
  (format nil "~A/~A" work-dir f))

(defun parse-org-file (f)
  (read-from-string (inferior-shell:run/s `(,(to-work-dir "org-parse.sh") ,f))))

(defun parse-file (f)
  (write-line (cl-json:encode-json-to-string (parse-org-file f))))

(defun main ()
  (parse-file (cadr *posix-argv*)))
