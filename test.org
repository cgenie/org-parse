#+title: Test


File with lots of types of elements.


* Level 1
** Level 2
*** Level 3
**** Level 4

* Markup
*bold* /italics/ _underline_ +strike+ =verbatim= ~code~

* Links
[[https://google.com][With description]]

Raw: [[https://google.com]]

* Todos
** TODO Task1
:LOGBOOK:
CLOCK: [2020-09-15 Tue 13:55]--[2020-09-15 Tue 13:56] =>  0:01
:END:
** DONE Task2
** TODO [#A] With priority
** TODO With deadline
DEADLINE: <2019-06-29 Sat>
