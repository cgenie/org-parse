##
# Project Title
#
# @file
# @version 0.1



# end

SBCL=/usr/bin/sbcl

all: parse.exe

parse.exe: parse.lisp
	$(SBCL) --load 'parse.lisp' --eval "(sb-ext:save-lisp-and-die \"parse.exe\" :toplevel #'main :executable t)"

clean:
	rm parse.exe
